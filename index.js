const express = require('express');
const bodyParser = require('body-parser');
const { json } = require('body-parser');
const app = express();

app.use(bodyParser.json());

let contactos = []
//middleware
let middleware1 = (req,res,next) => {
 let { name, email} = req.body
 console.log(name, email)
 if(name == "hola" || !email) {
     return res.status(400).json("Faltan datos")
 } else { 
     return next()}
}

//rutas
app.get('/contactos', (req, res) => {
    res.json(JSON.stringify(contactos));
});

app.post('/contactos',middleware1,(req,res) => {
   //console.log(req.body)
   contactos.push(req.body)
   const { method, path, query, body } = req
   
   res.send (`${method} , ${path} , ${JSON.stringify(query)},  ${JSON.stringify(body)}` )
   next()
   
});

app.listen(3000, () => {
    console.log('Servidor iniciado...');
});

